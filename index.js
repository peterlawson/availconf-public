var express = require('express');
var app = express();
app.use(express.static('public'));

var io = require('socket.io')(3030);

var bodyParser = require('body-parser');
app.use(bodyParser());

//// DB
// var LocalStorage = require('node-localstorage').LocalStorage;
// localStorage = new LocalStorage('./db');
//// DB

//// TWILIO
var accountSid = process.env.ACCOUNT_SID;
var authToken = process.env.AUTH_TOKEN;
var twilio_client = require('twilio')(accountSid, authToken);
var MessagingResponse = require('twilio').twiml.MessagingResponse;
//// TWILIO

// SOCKET.IO
io.on('connection', function(socket) {
  console.log('Socket connection established with ' + socket.id)
  socket.emit('connected', function() {
    console.log('connected to client')
  })
  
  socket.on('checkCustomerStatus', function(phoneNumber) {
    console.log('Sending SMS to ' + phoneNumber)
    twilio_client.messages.create({
      to: "+1" + phoneNumber,
      from: "+16157249953",
      body: "Hi, this is Jake's Bakes! We have a gift delivery for you! We've been instructed to deliver it to 123 Nashville Drive. Will you be able to accept the order there in about an hour? Reply 'Yes' or 'No' and we'll give you a call if we have any other questions!",
    })
  })
})

//// EXPRESS ROUTES
app.get('/', function(req, res) {
  res.render('index.html')
});

app.post('/confirm', function (req, res) {
  var phoneNum = req.body.phoneNumber;
  twilio_client.messages.create({
    to: "+1" + phoneNum,
    from: "+16157249953",
    body: "Hi, this is Jake's Bakes! We have a gift delivery for you! We've been instructed to deliver it to 123 Nashville Drive. Will you be able to accept the order there in about an hour? Reply 'Yes' or 'No' and we'll give you a call if we have any other questions!",
  })
});

app.post('/reply', (req, res) => {
  // console.log(req.body)
  console.log(req);

  var twiml = new MessagingResponse;

  if (req.body.Body == 'Yes') {
    twiml.message("Great! We'll see you soon!");
    io.emit("status", "available")
    // save response in DB
  } else if (req.body.Body == 'No') {
    twiml.message("Thanks for letting us know! We'll give you a call and try to find a better time!");
    io.emit("status", "not available")
    // save response in DB
  } else {
    twiml.message("Please reply either 'Yes' or 'No', or give us a call at (615) 645-5916. Thanks!");
  }

  res.writeHead(200, {'Content-Type': 'text/xml'});
  res.end(twiml.toString());
});
//// EXPRESS ROUTES

app.listen(3000, function() {
  console.log('Listening on port 3000')
});
